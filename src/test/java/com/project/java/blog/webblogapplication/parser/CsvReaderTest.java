package com.project.java.blog.webblogapplication.parser;

import com.opencsv.exceptions.CsvValidationException;
import com.project.java.blog.webblogapplication.domain.Comment;
import com.project.java.blog.webblogapplication.domain.Post;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvReaderTest {

    @Test
    public void testSaveCsv() throws IOException, CsvValidationException {
        ImmutablePair<String, List<Comment>> comments = new ImmutablePair<>("comments.csv",
                List.of(new Comment(1,"Zbyszek12",101, "Jedziemy z testami")));
        ImmutablePair<String, List<Post>> posts = new ImmutablePair<>("posts.csv", List.of(new Post(1, List.of("elo elo", "jedziemy"),
                "dobra jakis post", List.of("elo", "b"))));
        CsvReader csvReader = new CsvReader();
        csvReader.saveCsv(comments,posts);

        Pair<List<Comment>,List<Post>> resault = csvReader.readCsv("comments.csv","posts.csv");

        assertEquals(comments.getRight().get(0),resault.getLeft().get(0));
        assertEquals(posts.getRight().get(0),resault.getRight().get(0));
    }

}