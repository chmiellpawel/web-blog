package com.project.java.blog.webblogapplication.service;
import com.project.java.blog.webblogapplication.domain.Comments;
import com.project.java.blog.webblogapplication.domain.Posts;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseInitializer implements CommandLineRunner {

    Posts posts;
    Comments comments;
    CommentManager commentManager;
    PostManagerMongoRepository postManagerMongoRepository;

    public DatabaseInitializer(Posts posts, Comments comments, CommentManager commentManager, PostManagerMongoRepository postManagerMongoRepository) {
        this.posts = posts;
        this.comments = comments;
        this.commentManager = commentManager;
        this.postManagerMongoRepository = postManagerMongoRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        postManagerMongoRepository.deleteAll();
        commentManager.deleteAll();
        postManagerMongoRepository.saveAll(posts.getPostList());
        commentManager.saveAll(comments.getCommentList());

    }
}
