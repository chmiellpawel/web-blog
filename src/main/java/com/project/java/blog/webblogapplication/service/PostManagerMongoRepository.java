package com.project.java.blog.webblogapplication.service;

import com.project.java.blog.webblogapplication.domain.Post;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PostManagerMongoRepository extends MongoRepository<Post,Integer> {
}
