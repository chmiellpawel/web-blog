package com.project.java.blog.webblogapplication.service;

import java.util.List;

public interface PostManager {
    List<String> getTags();

    List<String> getAuthors();
}
