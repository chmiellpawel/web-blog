package com.project.java.blog.webblogapplication.controller.web;

import com.project.java.blog.webblogapplication.domain.Role;
import com.project.java.blog.webblogapplication.domain.User;
import com.project.java.blog.webblogapplication.service.UserManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class UserController {

    UserManager userManager;
    PasswordEncoder passwordEncoder;

    private UserController(UserManager userManager, PasswordEncoder passwordEncoder){
        this.userManager = userManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping(value = "/login")
    private String getLoginUser(Model model){
        model.addAttribute("user", new User());
        return "userLogin";
    }

    @PostMapping(value = "/login")
    private String postLoginUser(@Valid User user, Errors errors){
        if(errors.hasErrors())
        {
            return "redirect:/login";
        }
        User dbUser = userManager.findByUsername(user.getUsername());
        System.out.println(passwordEncoder.matches(user.getPassword(),dbUser.getPassword()));
        return "userLogin";
    }

    @GetMapping(value = "/register")
    private String getRegisterUser(Model model){
        model.addAttribute("user", new User());
        return "userRegister";
    }

    @GetMapping(value = "/register/{username}")
    private String getResterFromComment(Model model, @PathVariable String username){
        var user = new User();
        user.setUsername(username);
        model.addAttribute("user", user);
        return "userRegister";
    }

    @PostMapping(value = "/register")
    private String postRegisterUser(@Valid @ModelAttribute("user") User user, Errors errors){
        if(errors.hasErrors() || userManager.existsByUsername(user.getUsername()))
        {
            return "redirect:/register";
        }
        user.setId(UUID.randomUUID().toString());
        user.setRole(Role.USER);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userManager.save(user);
        return "redirect:/";
    }

}
