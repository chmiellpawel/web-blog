package com.project.java.blog.webblogapplication.domain;


import com.project.java.blog.webblogapplication.validator.Password;
import com.project.java.blog.webblogapplication.validator.Username;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.Email;

@Document(collection = "User")
public class User implements GrantedAuthority {
    @Id
    private String id;
    @Username
    private String username;
    @Email(message = "Podany email jest nieprawidłowy")
    private String email;
    @Password
    private String password;
    private Role role;

    public User() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role.toString();
    }
}
