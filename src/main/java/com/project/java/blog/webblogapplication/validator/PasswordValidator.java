package com.project.java.blog.webblogapplication.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    @Override
    public void initialize(Password constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        int passwordStrength = 0;
        if (value.length() < 8) {
            return false;
        } else if (value.length() >= 10) {
            passwordStrength += 2;
        } else {
            passwordStrength += 1;
        }
        if (value.matches("(?=.*[0-9]).*"))
            passwordStrength += 2;

        if (value.matches("(?=.*[a-z]).*"))
            passwordStrength += 2;

        if (value.matches("(?=.*[A-Z]).*"))
            passwordStrength += 2;

        if (value.matches("(?=.*[~!@#$%^&*()_-]).*"))
            passwordStrength += 2;
        return passwordStrength > 7;
    }
}
