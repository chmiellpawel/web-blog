package com.project.java.blog.webblogapplication.service;

import com.project.java.blog.webblogapplication.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserManager extends MongoRepository<User, Integer> {
    User findByUsername(String username);
    boolean existsByUsername(String username);
}
