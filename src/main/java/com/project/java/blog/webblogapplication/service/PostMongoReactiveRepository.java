package com.project.java.blog.webblogapplication.service;

import com.project.java.blog.webblogapplication.domain.Post;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

public interface PostMongoReactiveRepository extends ReactiveMongoRepository<Post,Integer> {
}
