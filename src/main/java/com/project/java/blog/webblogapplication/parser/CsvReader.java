package com.project.java.blog.webblogapplication.parser;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import com.project.java.blog.webblogapplication.domain.Comment;
import com.project.java.blog.webblogapplication.domain.Post;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvReader {

    public void saveCsv(Pair<String, List<Comment>> comments, Pair<String, List<Post>> posts) throws IOException {
        saveComments(comments);
        savePosts(posts);
    }

    public Pair<List<Comment>, List<Post>> readCsv(String commentPath, String postPath) throws IOException, CsvValidationException {
        List<Post> posts = getPostList(postPath);
        List<Comment> comments = getCommentList(commentPath);
        return new ImmutablePair<>(comments,posts);
    }

    private List<Post> getPostList(String postPath) throws IOException, CsvValidationException {
        CSVReader csvReader = new CSVReader(new FileReader(postPath));
        List<Post> posts = new ArrayList<>();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            Post post = new Post();
            post.setId(Integer.parseInt(line[0]));
            post.setAuthorsList(Arrays.asList(line[1].replace("[","").replace("]","").split(", ")));
            post.setPostContent(line[2]);
            post.setTagsList(Arrays.asList(line[3].replace("[","").replace("]","").split(", ")));
            posts.add(post);
        }
        return posts;
    }

    private List<Comment> getCommentList(String commentPath) throws IOException, CsvValidationException {
        CSVReader csvReader = new CSVReader(new FileReader(commentPath));
        List<Comment> comments = new ArrayList<>();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            Comment comment = new Comment();
            comment.setId(Integer.parseInt(line[0]));
            comment.setCommentContent(line[1]);
            comment.setIdPost(Integer.parseInt(line[2]));
            comment.setUsername(line[3]);
            comments.add(comment);
        }
        return comments;
    }

    private void saveComments(Pair<String, List<Comment>> comments) throws IOException {
        FileWriter out = new FileWriter(comments.getLeft());
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
            comments.getRight()
                    .forEach(comment -> {
                        try {
                            printer.printRecord(comment.getId(), comment.getCommentContent(), comment.getIdPost(), comment.getUsername());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }

    public void savePosts(Pair<String, List<Post>> posts) throws IOException {
        FileWriter out = new FileWriter(posts.getLeft());
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
            posts.getRight()
                    .forEach(post -> {
                        try {
                            printer.printRecord(post.getId(), post.getAuthorsList(), post.getPostContent(), post.getTagsList());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }
}
