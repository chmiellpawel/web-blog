package com.project.java.blog.webblogapplication.controller.api;

import com.project.java.blog.webblogapplication.domain.User;
import com.project.java.blog.webblogapplication.service.UserManager;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class UserControllerApi {

    private final UserManager userManager;

    public UserControllerApi(UserManager userManager) {
        this.userManager = userManager;
    }

    @PostMapping("/user/add")
    private ResponseEntity<User> addUser(@RequestBody User user){

        return ResponseEntity.ok(userManager.save(user));
    }
}
