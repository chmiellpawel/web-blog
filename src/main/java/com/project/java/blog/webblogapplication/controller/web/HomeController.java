package com.project.java.blog.webblogapplication.controller.web;

import com.project.java.blog.webblogapplication.domain.Authorities;
import com.project.java.blog.webblogapplication.domain.Post;
import com.project.java.blog.webblogapplication.service.PostManagerMongoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    private final PostManagerMongoRepository postManagerMongoRepository;
    private final Authorities authorities;

    public HomeController(PostManagerMongoRepository postManagerMongoRepository, Authorities authorities) {
        this.postManagerMongoRepository = postManagerMongoRepository;
        this.authorities = authorities;
    }

    @GetMapping("/")
    private String home(Model model) {
        model.addAttribute("posts", postManagerMongoRepository.findAll());
        model.addAttribute("role", authorities.getRole().toString());
        return "home";
    }

    @PostMapping("/")
    private String sortedHome(Model model, String sortValue) {
        List<Post> posts = new ArrayList<>(postManagerMongoRepository.findAll());
        System.out.println(sortValue);
        switch (sortValue) {
            case "Id descending": {
                posts.sort((a, b) -> b.getId().compareTo(a.getId()));
                model.addAttribute("posts", posts);
                break;
            }
            case "Post ascending": {
                posts.sort((a, b) -> a.getPostContent().compareToIgnoreCase(b.getPostContent()));
                model.addAttribute("posts", posts);
                break;
            }
            case "Post descending":
                posts.sort((a, b) -> b.getPostContent().compareToIgnoreCase(a.getPostContent()));
                model.addAttribute("posts", posts);
                break;

            case "Author ascending": {
                posts.sort((a, b) -> a.getAuthorsList().get(0).compareToIgnoreCase(b.getAuthorsList().get(0)));
                model.addAttribute("posts", posts);
                break;
            }
            case "Author descending": {
                posts.sort((a, b) -> b.getAuthorsList().get(0).compareToIgnoreCase(a.getAuthorsList().get(0)));
                model.addAttribute("posts", posts);
                break;
            }
            case "Tag ascending": {
                posts.sort((a, b) -> a.getTagsList().get(0).compareToIgnoreCase(b.getTagsList().get(0)));
                model.addAttribute("posts", posts);
                break;
            }
            case "Tag descending": {
                posts.sort((a, b) -> b.getTagsList().get(0).compareToIgnoreCase(a.getTagsList().get(0)));
                model.addAttribute("posts", posts);
                break;
            }
            default: {
                model.addAttribute("posts", posts);
                break;
            }
        }
        return "home";
    }

}
