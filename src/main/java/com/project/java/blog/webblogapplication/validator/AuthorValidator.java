package com.project.java.blog.webblogapplication.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class AuthorValidator implements ConstraintValidator<Author, List<String>> {

    @Override
    public void initialize(Author constraintAnnotation) {

    }

    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext context) {
        return value.size() > 0 && value.stream().allMatch(s -> s.matches("^[A-z]{3,}( [A-z']{2,})+$"));
    }
}
