package com.project.java.blog.webblogapplication.security;

import com.project.java.blog.webblogapplication.domain.User;
import com.project.java.blog.webblogapplication.service.UserManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class SecUserDetailsService implements UserDetailsService {

    private final UserManager userManager;

    public SecUserDetailsService(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userManager.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("Nie ma takiego usera");
        }
        return new SecUserDetails(user);
    }
}
