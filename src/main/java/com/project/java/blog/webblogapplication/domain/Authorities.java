package com.project.java.blog.webblogapplication.domain;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class Authorities {

    public Role getRole() {
        var authority = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        var simpleGrantedAuthority = authority.iterator().next();
        var role = simpleGrantedAuthority.getAuthority();
        if (role.equals("ROLE_ANONYMOUS")) {
            return Role.GUEST;
        }

        return Role.valueOf(role);
    }

    public String getUsername(){
        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            return ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

}
