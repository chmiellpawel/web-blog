package com.project.java.blog.webblogapplication.service;

import com.project.java.blog.webblogapplication.domain.Post;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PostManagerService implements PostManager{

    PostManagerMongoRepository postManagerMongoRepository;

    public PostManagerService(PostManagerMongoRepository postManagerMongoRepository) {
        this.postManagerMongoRepository = postManagerMongoRepository;
    }

    public List<String> getTags(){
        return postManagerMongoRepository.findAll().stream()
                .map(Post::getTagsList)
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAuthors() {
        return postManagerMongoRepository.findAll().stream()
                .map(Post::getAuthorsList)
                .flatMap(Collection::stream)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }
}
