package com.project.java.blog.webblogapplication.controller.web;

import com.project.java.blog.webblogapplication.domain.Comment;
import com.project.java.blog.webblogapplication.service.CommentManager;
import com.project.java.blog.webblogapplication.service.PostManagerMongoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.stream.Collectors;

@Controller
public class CommentController {

    private final CommentManager commentManager;
    private final PostManagerMongoRepository postManagerMongoRepository;

    public CommentController(CommentManager commentManager, PostManagerMongoRepository postManagerMongoRepository) {
        this.commentManager = commentManager;
        this.postManagerMongoRepository = postManagerMongoRepository;
    }

    @GetMapping(value = "/post/{idPost}/comment/{idComment}/delete")
    private String deleteComment(@PathVariable Integer idComment) {
        commentManager.deleteById(idComment);
        return "redirect:/post/{idPost}";
    }

    @GetMapping(value = "/post/{idPost}/comment/{idComment}/edit")
    private String editCommentGet(@PathVariable Integer idComment, Model model, @PathVariable Integer idPost) {
        model.addAttribute("comment", commentManager.findById(idComment).get());
        model.addAttribute("post", postManagerMongoRepository.findById(idPost).get());
        return "commentEdit";
    }

    @PostMapping(value = "/post/{idPost}/comment/idComment/edit")
    private String editCommentPost(Comment comment) {
        commentManager.save(comment);
        return "redirect:/post/{idPost}";
    }

    @GetMapping(value = "/comment/user")
    private String findUserCommentsGet() {
        return "commentFind";
    }

    @PostMapping(value = "/comment/user")
    private String findUserCommentsPost(String username, Model model) {
        model.addAttribute("comments", commentManager.findAll()
                .stream()
                .filter(c -> c.getUsername().equals(username))
                .collect(Collectors.toList())
        );
        return "commentFind";
    }


}
