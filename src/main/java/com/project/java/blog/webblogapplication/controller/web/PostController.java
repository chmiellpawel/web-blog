package com.project.java.blog.webblogapplication.controller.web;

import com.project.java.blog.webblogapplication.domain.Authorities;
import com.project.java.blog.webblogapplication.domain.Comment;
import com.project.java.blog.webblogapplication.domain.Post;
import com.project.java.blog.webblogapplication.service.CommentManager;
import com.project.java.blog.webblogapplication.service.PostManager;
import com.project.java.blog.webblogapplication.service.PostManagerMongoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class PostController {

    private final CommentManager commentManager;
    private final PostManagerMongoRepository postManagerMongoRepository;
    private final PostManager postManager;
    private Integer idPostNumber;
    private Integer idCommentNumber;
    private final Authorities authorities;

    public PostController(CommentManager commentManager, PostManagerMongoRepository postManagerMongoRepository, PostManager postManager, Authorities authorities) {
        this.commentManager = commentManager;
        this.postManagerMongoRepository = postManagerMongoRepository;
        this.idPostNumber = postManagerMongoRepository.findAll().size();
        this.idCommentNumber = commentManager.findAll().size();
        this.postManager = postManager;
        this.authorities = authorities;
    }

    @GetMapping(value = "/post/create")
    private String createPostGet(Model model) {
        model.addAttribute("post", new Post());
        model.addAttribute("tagsList", postManager.getTags());
        model.addAttribute("authorsList", postManager.getAuthors());
        return "postCreate";
    }

    @PostMapping(value = "/post/create")
    private String createPostPost(@Valid Post post, Errors errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("tagsList", postManager.getTags());
            model.addAttribute("authorsList", postManager.getAuthors());
            return "postCreate";
        }
        post.setId(++idPostNumber);
        postManagerMongoRepository.save(post);
        return "redirect:/";
    }

    @GetMapping(value = "/post/delete/{id}")
    private String deletePost(@PathVariable Integer id) {
        postManagerMongoRepository.deleteById(id);
        List<Comment> commentList = commentManager.getCommentsByIdPost(id);
        for(Comment comment : commentList){
            commentManager.delete(comment);
        }

        return "redirect:/";
    }

    @GetMapping(value = "/post/edit/{id}")
    private String editPostGet(@PathVariable Integer id, Model model) {
        model.addAttribute("post", postManagerMongoRepository.findById(id).get());
        return "postEdit";
    }

    @PostMapping(value = "/post/edit/{id}")
    private String updatePostPost(Post post) {
        postManagerMongoRepository.save(post);
        return "redirect:/";
    }

    @GetMapping(value = "/post/{id}")
    private String getPost(@PathVariable Integer id, Model model) {
        List<Comment> commentsToPost = new ArrayList<>();
        Optional<Post> post = postManagerMongoRepository.findById(id);
        model.addAttribute("post", post.get());
        for (Comment comment : commentManager.findAll()) {
            if (comment.getIdPost().equals(id)) {
                commentsToPost.add(comment);
            }
        }
        model.addAttribute("comments", commentsToPost);
        model.addAttribute("comment", new Comment());
        model.addAttribute("role", authorities.getRole().toString());
        model.addAttribute("username", authorities.getUsername());
        return "post";
    }

    @PostMapping(value = "/post/{id}")
    private String getPostPost(@PathVariable Integer id, @Valid Comment comment, Errors errors) {
        if (errors.hasErrors()) {
            return "redirect:/post/{id}";
        }
        comment.setId(++idCommentNumber);
        comment.setIdPost(id);
        commentManager.save(comment);
        return "redirect:/post/{id}";
    }

    @GetMapping("/post/search")
    private String searchPostsGet() {
        return "postFind";
    }

    @PostMapping("/post/search")
    private String searchPostsPost(Model model, String postPattern, String findType) {
        switch (findType) {
            case "post": {
                model.addAttribute("posts", postManagerMongoRepository.findAll()
                        .stream()
                        .filter(a -> a.getPostContent().contains(postPattern))
                        .collect(Collectors.toList())
                );
                break;
            }
            case "author": {
                model.addAttribute("posts", postManagerMongoRepository.findAll()
                        .stream()
                        .filter(a -> a.getAuthorsList().contains(postPattern))
                        .collect(Collectors.toList())
                );
                break;
            }
            case "tag": {
                model.addAttribute("posts", postManagerMongoRepository.findAll()
                        .stream()
                        .filter(a -> a.getTagsList().contains(postPattern))
                        .collect(Collectors.toList())
                );
                break;
            }
        }
        return "postFind";
    }

}
