package com.project.java.blog.webblogapplication.service;

import com.project.java.blog.webblogapplication.domain.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CommentManager extends MongoRepository<Comment,Integer> {
    List<Comment> getCommentsByIdPost(Integer idPost);
}
