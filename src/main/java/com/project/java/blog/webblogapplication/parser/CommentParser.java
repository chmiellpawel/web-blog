package com.project.java.blog.webblogapplication.parser;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import com.project.java.blog.webblogapplication.domain.Comment;
import lombok.Data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public @Data class CommentParser {

    private final String CSV_FILE = "csv/Comments.csv";
    private final CSVReader CSV_READER = new CSVReader(new FileReader(CSV_FILE));
    private final String XML_FILE = "src/main/resources/comments.xml";

    private List<Comment> commentList = new ArrayList<>();

    public CommentParser() throws FileNotFoundException {
    }

    public void commentLoader(){
        try{
            String[] line = this.getCSV_READER().readNext();
            while((line = this.getCSV_READER().readNext()) != null){
                Comment comment = new Comment();
                comment.setId(Integer.valueOf(line[0]));
                comment.setUsername(line[1]);
                comment.setIdPost(Integer.valueOf(line[2]));
                comment.setCommentContent(line[3]);
                this.getCommentList().add(comment);
            }

        } catch (CsvValidationException | IOException e) {
            e.printStackTrace();
        }
    }

    public void listPrinter(){
        for(Comment comment : commentList){
            System.out.println(comment);
        }
    }

    public void xmlBeanSaver(){
        try {
            PrintWriter printWriter = new PrintWriter(XML_FILE);
            printWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
                    "       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                    "       xmlns:context=\"http://www.springframework.org/schema/context\"\n" +
                    "       xsi:schemaLocation=\"http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd " +
                    "http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.0.xsd\">\n");
            for(Comment comment : this.getCommentList()){
                printWriter.println("\t<bean id=\"" + comment.getUsername() + comment.getId() + "post" +"\" class=\"com.project.java.blog.webblogapplication.domain.Comment\">\n" +
                        "\t\t<property name=\"id\" value=\"" + comment.getId() + "\" />\n" +
                        "\t\t<property name=\"username\" value=\"" + comment.getUsername() + "\" />\n" +
                        "\t\t<property name=\"idPost\" value=\"" + comment.getIdPost() + "\" />\n" +
                        "\t\t<property name=\"commentContent\" value=\"" + comment.getCommentContent() + "\" />\n" +
                        "\t</bean>");
            }
            printWriter.println("\t<bean id=\"comments\" class=\"com.project.java.blog.webblogapplication.domain.Comments\">\n" +
                    "\t\t<property name=\"commentList\">\n" +
                    "\t\t\t<list>");
            for(Comment comment : this.getCommentList()){
                printWriter.println("\t\t\t\t<ref bean=\"" + comment.getUsername() + comment.getId() + "post\"/>");
            }
            printWriter.println("\t\t\t</list>\n" +
                    "\t\t</property>\n" +
                    "\t</bean>");
            printWriter.println("</beans>");
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
