package com.project.java.blog.webblogapplication.domain;

import com.project.java.blog.webblogapplication.validator.Author;
import com.project.java.blog.webblogapplication.validator.Tag;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public @Document(collection = "Post")
@NoArgsConstructor @AllArgsConstructor
@Data class Post {
    @Id
    private Integer id;

    @Author
    private List<String> authorsList = new ArrayList<>();

    @NotNull
    @Size(min = 30, message = "Post musi zawierać przynajmniej 30 znaków")
    private String postContent;

    @Tag
    private List<String> tagsList = new ArrayList<>();

}
