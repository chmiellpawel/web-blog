package com.project.java.blog.webblogapplication.parser;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import com.project.java.blog.webblogapplication.domain.Comment;
import com.project.java.blog.webblogapplication.domain.Post;
import lombok.Data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public @Data
class PostParser {
    private final String CSV_FILE = "csv/ManyPostsManyAuthors.csv";
    private final String AUTHOR_REGEX = "[A-z'-]* [A-z'-]* ?[A-z'-]* ?[A-z'-]*";
    private final CSVReader CSV_READER = new CSVReader(new FileReader(CSV_FILE));
    private final String XML_FILE = "src/main/resources/posts.xml";

    private Pattern authorPattern = Pattern.compile(AUTHOR_REGEX);
    private Matcher authorMatcher;
    private List<Post> postList = new ArrayList<>();

    public PostParser() throws FileNotFoundException {
    }


    public void postLoader() {
        try {

            String[] line = this.getCSV_READER().readNext();
            while ((line = this.getCSV_READER().readNext()) != null) {
                Post post = new Post();
                post.setId(Integer.parseInt(line[0]));
                this.setAuthorMatcher(this.getAuthorPattern().matcher(line[1]));
                while (this.getAuthorMatcher().find()) {
                    String name = this.authorMatcher.group();
                    post.getAuthorsList().add(name);
                }
                post.setPostContent(line[2].replaceAll("\\s+", " "));
                post.setTagsList(Arrays.asList(line[3].split(" ")));
                this.postList.add(post);
            }

        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
    }


    public void printList() {
        for (Post post : postList) {
            System.out.println(post);
        }
    }

    public void saveObjectsAsXmlBeans() {
        try {
            PrintWriter printWriter = new PrintWriter(XML_FILE);
            printWriter.println("<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
                    "\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                    "\txmlns:util=\"http://www.springframework.org/schema/util\"\n" +
                    "\txsi:schemaLocation=\"http://www.springframework.org/schema/beans\n" +
                    "           http://www.springframework.org/schema/beans/spring-beans.xsd\n" +
                    "           http://www.springframework.org/schema/util \n" +
                    "   \t\t   http://www.springframework.org/schema/util/spring-util.xsd\">\n");
            for (Post post : this.getPostList()) {
                printWriter.println("<bean id=\"" + "postNumber" + post.getId() + "\" class=\"com.project.java.blog.webblogapplication.domain.Post\">\n" +
                        "\t<property name=\"id\" value=\"" + post.getId() + "\" />\n" +
                        "\t<property name=\"authorsList\">\n" +
                        "\t\t<util:list value-type=\"java.lang.String\">");
                for (String author : post.getAuthorsList()) {
                    printWriter.println("\t\t\t<value>" + author + "</value>");
                }
                printWriter.println("\t\t</util:list>\n" +
                        "\t</property>\n" +
                        "\t<property name=\"postContent\" value=\"" + post.getPostContent() + "\" />\n" +
                        "\t<property name=\"tagsList\">\n" +
                        "\t\t<util:list value-type=\"java.lang.String\">");
                for (String tag : post.getTagsList()) {
                    printWriter.println("\t\t\t<value>" + tag + "</value>");
                }
                printWriter.println("\t\t</util:list>\n" +
                        "\t</property>\n" +
                        "</bean>");
            }
            printWriter.println("\t<bean id=\"posts\" class=\"com.project.java.blog.webblogapplication.domain.Posts\">\n" +
                    "\t\t<property name=\"postList\">\n" +
                    "\t\t\t<list>");
            for (Post post : this.getPostList()) {
                printWriter.println("\t\t\t\t<ref bean=\"" + "postNumber" + post.getId() + "\"/>");
            }
            printWriter.println("\t\t\t</list>\n" +
                    "\t\t</property>\n" +
                    "\t</bean>");
            printWriter.println("</beans>");
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
