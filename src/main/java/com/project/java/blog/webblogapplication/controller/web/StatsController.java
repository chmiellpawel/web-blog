package com.project.java.blog.webblogapplication.controller.web;

import com.project.java.blog.webblogapplication.service.CommentManager;
import com.project.java.blog.webblogapplication.service.PostManagerMongoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class StatsController {

    private final PostManagerMongoRepository postManagerMongoRepository;
    private final CommentManager commentManager;

    public StatsController(PostManagerMongoRepository postManagerMongoRepository, CommentManager commentManager) {
        this.postManagerMongoRepository = postManagerMongoRepository;
        this.commentManager = commentManager;
    }

    @GetMapping(value = "/stats")
    private String getStats(Model model) {
        return "stats";
    }

    @PostMapping(value = "/stats")
    private String getStatsPost(String type, String name, Model model) {
        model.addAttribute("name", name);
        if (type.equals("user")) {
            model.addAttribute("numberOfActions", commentManager.findAll()
                    .stream()
                    .filter(c -> c.getUsername().equals(name))
                    .count()
            );
        } else {
            model.addAttribute("numberOfActions", postManagerMongoRepository.findAll()
                    .stream()
                    .filter(c -> c.getAuthorsList().contains(name))
                    .count()
            );
        }
        return "stats";
    }


}
