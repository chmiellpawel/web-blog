package com.project.java.blog.webblogapplication.validator;

import com.project.java.blog.webblogapplication.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class UsernameValidator implements ConstraintValidator<Username, String> {


    @Override
    public void initialize(Username constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.matches("^[A-z0-9]{7,}$");
    }
}
