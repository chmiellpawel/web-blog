package com.project.java.blog.webblogapplication.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AuthorValidator.class)
public @interface Author {

    String message() default "Musisz podać chociaż jednego autora";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
