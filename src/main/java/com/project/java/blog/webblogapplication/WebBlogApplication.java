package com.project.java.blog.webblogapplication;

import com.project.java.blog.webblogapplication.parser.CommentParser;
import com.project.java.blog.webblogapplication.parser.PostParser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations = {"classpath*:posts.xml","classpath*:comments.xml"})

@SpringBootApplication
public class WebBlogApplication {

	public static void main(String[] args) throws Exception {
		PostParser postParser = new PostParser();
		postParser.postLoader();
		postParser.saveObjectsAsXmlBeans();
		CommentParser commentParser = new CommentParser();
		commentParser.commentLoader();
		commentParser.xmlBeanSaver();
		SpringApplication.run(WebBlogApplication.class, args);

	}

}
