package com.project.java.blog.webblogapplication.domain;

import com.project.java.blog.webblogapplication.validator.Username;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public @Document(collection = "Comment")
@NoArgsConstructor @AllArgsConstructor
@Data
class Comment {
    @Id
    private Integer id;
    @Username
    private String username;
    private Integer idPost;
    @NotNull
    @Size(min = 10)
    private String commentContent;

    public Comment(String username){
        this.username = username;
    }
}

