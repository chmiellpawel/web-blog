package com.project.java.blog.webblogapplication.domain;

public enum Role {
    GUEST,
    USER,
    ADMIN
}
