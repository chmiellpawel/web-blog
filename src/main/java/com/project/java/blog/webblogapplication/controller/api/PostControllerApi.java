package com.project.java.blog.webblogapplication.controller.api;

import com.project.java.blog.webblogapplication.domain.Post;
import com.project.java.blog.webblogapplication.service.PostMongoReactiveRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
public class PostControllerApi {

    private final PostMongoReactiveRepository postMongoReactiveRepository;

    public PostControllerApi(PostMongoReactiveRepository postMongoReactiveRepository) {
        this.postMongoReactiveRepository = postMongoReactiveRepository;
    }

    @GetMapping(value = "/api/post/{id}")
    ResponseEntity<Mono<Post>> getPost(@PathVariable Integer id){
        return ResponseEntity.ok(postMongoReactiveRepository.findById(id));
    }

    @GetMapping(value="/api/post")
    ResponseEntity<Flux<Post>> getPosts(){
        return ResponseEntity.ok(postMongoReactiveRepository.findAll());
    }

}
